FROM docker.io/fedora:27
USER root

WORKDIR /root

RUN mkdir -p Hugo/share

VOLUME ["/root/Hugo/share"]


RUN curl https://copr.fedorainfracloud.org/coprs/daftaupe/hugo/repo/fedora-27/daftaupe-hugo-fedora-27.repo > /etc/yum.repos.d/hugo.repo

RUN dnf clean all
RUN dnf -y install git net-tools hugo

RUN echo "alias vi='vim'" > /etc/profile && source /etc/profile

RUN cd Hugo && hugo new site Template && cd Template \
        && git clone https://github.com/gcushen/hugo-academic.git ./themes/academic \
        && cp -a themes/academic/exampleSite/* .

CMD ["/bin/bash"]
